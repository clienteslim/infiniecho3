package com.hoon.infiniecho3.domain.member;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMember is a Querydsl query type for Member
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QMember extends EntityPathBase<Member> {

    private static final long serialVersionUID = -2080537630L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMember member = new QMember("member1");

    public final NumberPath<Integer> contLoginFailCt = createNumber("contLoginFailCt", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> idExpDate = createDateTime("idExpDate", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> idQuitTad = createDateTime("idQuitTad", java.time.LocalDateTime.class);

    public final StringPath isIdDisabled = createString("isIdDisabled");

    public final StringPath isIdExp = createString("isIdExp");

    public final StringPath isIdLock = createString("isIdLock");

    public final StringPath isIdQuit = createString("isIdQuit");

    public final StringPath isTempPwd = createString("isTempPwd");

    public final QMemberRole memberRoleList;

    public final DateTimePath<java.time.LocalDateTime> pwdChgTad = createDateTime("pwdChgTad", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> pwdExpDate = createDateTime("pwdExpDate", java.time.LocalDateTime.class);

    public final NumberPath<Integer> totLoginCt = createNumber("totLoginCt", Integer.class);

    public final StringPath userId = createString("userId");

    public final StringPath userName = createString("userName");

    public final NumberPath<Integer> userNo = createNumber("userNo", Integer.class);

    public final StringPath userPwd = createString("userPwd");

    public final DateTimePath<java.time.LocalDateTime> userRegTad = createDateTime("userRegTad", java.time.LocalDateTime.class);

    public QMember(String variable) {
        this(Member.class, forVariable(variable), INITS);
    }

    public QMember(Path<? extends Member> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMember(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMember(PathMetadata metadata, PathInits inits) {
        this(Member.class, metadata, inits);
    }

    public QMember(Class<? extends Member> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.memberRoleList = inits.isInitialized("memberRoleList") ? new QMemberRole(forProperty("memberRoleList"), inits.get("memberRoleList")) : null;
    }

}

