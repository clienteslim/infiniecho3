package com.hoon.infiniecho3.domain.comment;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QComment is a Querydsl query type for Comment
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QComment extends EntityPathBase<Comment> {

    private static final long serialVersionUID = 1109812228L;

    public static final QComment comment = new QComment("comment");

    public final StringPath commentActSt = createString("commentActSt");

    public final StringPath commentCont = createString("commentCont");

    public final NumberPath<Integer> commentId = createNumber("commentId", Integer.class);

    public final NumberPath<Integer> commentLv = createNumber("commentLv", Integer.class);

    public final NumberPath<Integer> commentParentId = createNumber("commentParentId", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> commentRegTad = createDateTime("commentRegTad", java.time.LocalDateTime.class);

    public final NumberPath<Integer> commentSqInGroup = createNumber("commentSqInGroup", Integer.class);

    public QComment(String variable) {
        super(Comment.class, forVariable(variable));
    }

    public QComment(Path<? extends Comment> path) {
        super(path.getType(), path.getMetadata());
    }

    public QComment(PathMetadata metadata) {
        super(Comment.class, metadata);
    }

}

