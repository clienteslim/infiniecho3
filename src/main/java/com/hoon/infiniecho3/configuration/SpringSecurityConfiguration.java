package com.hoon.infiniecho3.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@EnableWebSecurity
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter{

	private final UserDetailsService memberService;


	/*1. 암호화 처리를 위한 PasswordEncoder를 bean으로 설정 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		
		return new BCryptPasswordEncoder();
	}
	
	/*2. 시큐리티 설정을 무시할 정적 리소스들을 등록 (WebSecurityCongifurerAdapter의 메소드 오버라이딩(상속) */	
	@Override
	public void configure(WebSecurity web) throws Exception {
	
		web.ignoring().antMatchers("/css/**", "/js/**", "/images/**", "/lib/**");
	}
	
	
	/*3. Http 요청에 대한 권한 설정*/	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		/*csrf : 토큰 위조 공격을 막기 위한 것 (default가 'on'인 상태) */
		http.csrf().disable()					                                    //* 많은 사용자가 사용하는 서비스 경우 쿠키-세션은 동시접속과 인증-인가에 대해 많은 DB서버를 필요로하므로 이를 대신해 토큰을 사용
		                                                                            //* 하지만 토큰은 보안상의 단점이 있고, 이를 보완하기 위해서는 JWT(access token + refresh토큰)나 oAuth를 갖춰야함. 
		    /* 경로에 대한 권한 처리*/                                                    //* 해당 프로젝트는 대용량 서비스가 아니므로 보안상 세션만 사용 하기 위해 토큰사용을 위한 csrf는 disable함.		                                        
		    .authorizeRequests()                                                    // 요청에 대한 권한 체크를 어떻게 할 것인가 
		         .antMatchers("/menu/**").authenticated()                           // /menu/**에 대해서는 하나하나 권한을 등록하겠다.
		         .antMatchers(HttpMethod.GET, "/menu/**").hasRole("MEMBER")         // hasRole은 ROLE_을 달아주며 ROLE_MEMBER와 일치하면 허용하겠다.
		         .antMatchers(HttpMethod.POST, "/menu/**").hasRole("ADMIN")        
		         .antMatchers("/admin/**").hasRole("ADMIN")
		         .anyRequest().permitAll()                                          // 등록되지 않은 경로는 누구나 접근 가능
		    /* 로그인 처리 */     
		    .and()
		         .formLogin()
		         .loginPage("/member/login")                                        // 스프링 시큐리티 기본 로그인 form이 아닌 별도 로그인 form의 경로 지정
		         																	// (권한이 획득되지 않아 로그인이 필요한 상황에도 사용할 수 있게 함)
		         .successForwardUrl("/")                                            // 본래 이러한 경로를 별도 로그인 form 경로로 계승함
		    /* 로그아웃 처리 */     
		    .and()
		         .logout()
		         .logoutRequestMatcher(new AntPathRequestMatcher("/member/logout"))
		         .deleteCookies("JSESSIONID")
		         .invalidateHttpSession(true)
		         .logoutSuccessUrl("/")
		    /* 접속 거부 처리 */     
		    .and()
		         .exceptionHandling()
		         .accessDeniedPage("/common/denied");
		
	}
	
	
	/* 4. 권한을 획득할 때 인증할 비즈니스 로직(DB에서 조회하는 로직)이 어떤 것인지 등록 (Controller 없이 어떤 비지니스 로직을 사용할지만 설정하면 된다) */
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	
		auth.userDetailsService(memberService).passwordEncoder(passwordEncoder());
	}
	
}
