package com.hoon.infiniecho3.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.hoon.infiniecho3")
@EnableJpaRepositories(basePackages = "com.hoon.infiniecho3.domain") 
@EntityScan(basePackages = "com.hoon.infiniecho3.domain")
public class Infiniecho3Application {

	public static void main(String[] args) {
		SpringApplication.run(Infiniecho3Application.class, args);
	}

}
