package com.hoon.infiniecho3.service.member;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.hoon.infiniecho3.domain.member.Auth;
import com.hoon.infiniecho3.domain.member.Member;
import com.hoon.infiniecho3.domain.member.MemberDetail;
import com.hoon.infiniecho3.domain.member.MemberRepository;
import com.hoon.infiniecho3.domain.member.MemberRepositoryImpl;
import com.hoon.infiniecho3.domain.member.MemberRole;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class MemberService implements UserDetailsService{
	
	//private final MemberRepository repository;
	private final MemberRepositoryImpl repositoryImpl;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		System.out.println("test1");
		
		//Member member = repository.findMemberById(username);
		Member member = repositoryImpl.findMemberById(username);
		
		System.out.println("test2");
		if(member == null) {
			
			member = new Member();
			
			System.out.println("test3");
		}
		
		List<GrantedAuthority> authorities = new ArrayList<>();
		
		System.out.println("test4");
		
		if(member.getMemberRoleList() !=null) {
			
			List<MemberRole> roleList = member.getMemberRoleList();
			
			for(int i = 0; i < roleList.size(); i++) {
				
				Auth auth = roleList.get(i).getAuth();
				
				authorities.add(new SimpleGrantedAuthority(auth.getAuthName()));
				
				System.out.println("authorities : " + authorities);
				
			}
			
			
		}
		
		MemberDetail md = new MemberDetail(member, member.getUserId(), member.getUserPwd(), authorities);
		
		return md;
	}

}
