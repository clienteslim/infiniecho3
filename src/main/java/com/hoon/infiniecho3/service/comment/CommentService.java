package com.hoon.infiniecho3.service.comment;

import java.util.List;


import org.springframework.stereotype.Service;

import com.hoon.infiniecho3.domain.comment.Comment;
import com.hoon.infiniecho3.domain.comment.CommentRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class CommentService{

	private final CommentRepository repository;
		
	public List<Comment> findAll() {
				
		return repository.findAll();
	}
	
}
