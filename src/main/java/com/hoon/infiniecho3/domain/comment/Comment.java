package com.hoon.infiniecho3.domain.comment;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Entity
@Table(name="I02_COMMENT_TB")
public class Comment {

	@Id	
	private int commentId;
	private String commentCont;	
	private Integer commentParentId;
	private int commentLv;
	private Integer commentSqInGroup;		//nullable : null 값을 넣으려면 int가 아닌 INTEGER를 써야함.
	private LocalDateTime commentRegTad;
	private String commentActSt;

	@Builder
	public Comment(int commentId, String commentCont, Integer commentParentId, int commentLv, Integer commentSqInGroup,
			LocalDateTime commentRegTad, String commentActSt) {
		super();
		this.commentId = commentId;
		this.commentCont = commentCont;
		this.commentParentId = commentParentId;
		this.commentLv = commentLv;
		this.commentSqInGroup = commentSqInGroup;
		this.commentRegTad = commentRegTad;
		this.commentActSt = commentActSt;
	}
	
	

	
}
