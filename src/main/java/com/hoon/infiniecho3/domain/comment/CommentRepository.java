package com.hoon.infiniecho3.domain.comment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
	
	//@Query("select c from Comment c")
	List<Comment> findAll();
	
}
