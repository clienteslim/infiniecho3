package com.hoon.infiniecho3.domain.member;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;



public class MemberDetail extends User{

	@Autowired
	private final Member member;

	public MemberDetail(Member member, String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.member = member;
	}
	
}
