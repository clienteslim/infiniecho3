package com.hoon.infiniecho3.domain.member;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Entity
@Table(name="I01_AUTH_TB")
public class Auth {
	
	@Id
	private int authCode;
	private String authName;
	private String authDetail;
	
	
	@Builder
	public Auth(int authCode, String authName, String authDetail) {
		super();
		this.authCode = authCode;
		this.authName = authName;
		this.authDetail = authDetail;
	}
	
	
}
