package com.hoon.infiniecho3.domain.member;


import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class MemberRepositoryImpl extends QuerydslRepositorySupport{

	private final JPAQueryFactory queryFactory;
	
	public MemberRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
		super(Member.class);
		this.queryFactory = jpaQueryFactory;
	}
	
	public Member findMemberById(String username) {
		
		QMember member = QMember.member;
		QMemberRole memberRole = QMemberRole.memberRole;
		
		return queryFactory
			   .selectFrom(member)
			   .join(memberRole).on(member.userNo.eq(memberRole.userNo))
			   .where(member.userId.eq(username))			   
			   .fetchOne();
	} 
	
}
