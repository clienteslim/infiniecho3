package com.hoon.infiniecho3.domain.member;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Entity
@Table(name="I01_MEMBER_TB")
public class Member {
	
	@Id
	private int userNo;
	private String userId;
	private String userPwd;
	private LocalDateTime pwdChgTad;
	private LocalDateTime pwdExpDate;
	private String isTempPwd;
	private String userName;
	private LocalDateTime userRegTad;	
	private Integer totLoginCt;
	private Integer contLoginFailCt; 
	private String isIdLock;
	private String isIdDisabled;
	private LocalDateTime idExpDate;
	private String isIdExp;
	private LocalDateTime idQuitTad;
	private String isIdQuit;
	
	@ElementCollection
	@Transient
	@ManyToOne(targetEntity=MemberRole.class, fetch=FetchType.EAGER)
	private List<MemberRole> memberRoleList = new ArrayList<>();
	
	@Builder
	public Member(int userNo, String userId, String userPwd, LocalDateTime pwdChgTad, LocalDateTime pwdExpDate,
			String isTempPwd, String userName, LocalDateTime userRegTad, Integer totLoginCt, Integer contLoginFailCt,
			String isIdLock, String isIdDisabled, LocalDateTime idExpDate, String isIdExp, LocalDateTime idQuitTad,
			String isIdQuit
			, List<MemberRole> memberRoleList) {
		super();
		this.userNo = userNo;
		this.userId = userId;
		this.userPwd = userPwd;
		this.pwdChgTad = pwdChgTad;
		this.pwdExpDate = pwdExpDate;
		this.isTempPwd = isTempPwd;
		this.userName = userName;
		this.userRegTad = userRegTad;
		this.totLoginCt = totLoginCt;
		this.contLoginFailCt = contLoginFailCt;
		this.isIdLock = isIdLock;
		this.isIdDisabled = isIdDisabled;
		this.idExpDate = idExpDate;
		this.isIdExp = isIdExp;
		this.idQuitTad = idQuitTad;
		this.isIdQuit = isIdQuit;
		this.memberRoleList = memberRoleList;
	}
	
	
	
}
