package com.hoon.infiniecho3.domain.member;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


public interface MemberRepository extends JpaRepository<Member, Integer>{
	
	//@Query("select m from Member m join fetch MemberRole userNo where userId = :username")
	Member findMemberById(@Param("username") String username);
	
}
