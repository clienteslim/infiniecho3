package com.hoon.infiniecho3.domain.member;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Entity
@Table(name="I01_MEMBER_ROLE_TB")
public class MemberRole {
	
	@Id
	private int userNo;
	private int authCode;
	
	@Transient /* 테이블에서 Column으로 쓰지 않는 변수를 Entity에 선언할 때 사용 */
	@ManyToOne(targetEntity=Auth.class, fetch=FetchType.EAGER)
	private Auth auth;
	
	@Builder
	public MemberRole(int userNo, int authCode) {
		super();
		this.userNo = userNo;
		this.authCode = authCode;
	}
	
	
}
