package com.hoon.infiniecho3.controller.common;

import org.springframework.stereotype.Controller;

@Controller
public class DeniedController {
	
	public String denied() {
		
		return "common/denied";
	}
}
