package com.hoon.infiniecho3.controller.board;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/board/boardDetail")
public class boardDetailController {
	
	@GetMapping()
	public String home() {
		
		return "board/boardDetail";
	}
	
}
