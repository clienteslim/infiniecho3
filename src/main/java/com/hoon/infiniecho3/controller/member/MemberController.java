package com.hoon.infiniecho3.controller.member;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hoon.infiniecho3.domain.member.Member;
import com.hoon.infiniecho3.service.member.MemberService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Controller
@RequestMapping("/member")
public class MemberController {

	private final MemberService service;

	/*
	 * @PostMapping("/login") public UserDetails
	 * memberLoginForm(@RequestParam("username") String username) {
	 * 
	 * System.out.println(username);
	 * 
	 * return service.loadUserByUsername(username); }
	 */

	
	  @GetMapping("/login") 
	  public UserDetails memberLogin(@RequestParam("username") String username) {
	  
		  System.out.println(username);
		  
		  System.out.println(service.loadUserByUsername(username));
		  
		  return service.loadUserByUsername(username); 
	  
	  }
	 

}
