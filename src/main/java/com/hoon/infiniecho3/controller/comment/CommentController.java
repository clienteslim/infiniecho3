package com.hoon.infiniecho3.controller.comment;

import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hoon.infiniecho3.domain.comment.Comment;
import com.hoon.infiniecho3.service.comment.CommentService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class CommentController{
	
	private final CommentService service;
	
	@PostMapping("/comment/list")
	public List<Comment> findAll() {
		
		return service.findAll();
	}
	
	
	
}
